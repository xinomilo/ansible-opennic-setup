```forked from https://github.com/hack13/ansible-opennic-setup```

# OpenNIC Setup with Ansible
This is a simple Ansible Script to use to setup an OpenNIC server with Ansible. It
currently supports Devuan 3, Debian 10 and Ubuntu 18.04 LTS. I do plan to add Devuan 4 support in the future. 
It is also an open project, so feel free to fork and make your own changes.
I also welcome feedback.

# How To Use
On your local machine(or your Ansible host), and update your hosts file, like below updating with the IP Addresses of your servers:
```
[opennic]
xxx.xxx.xxx.xxx
xxx.xxx.xxx.xxx
```
Once you have added the hosts to your ansible hosts file, run the following command in the directory you have dropped this playbook:
```
ansible-playbook -i /path/to/hosts opennic-setup.yml
```

Test your opennic server here : 
http://report.opennicproject.org/t2log/t2.php

# Notes For Each OS

## Ubuntu 18.04 LTS
* **NOTE:** Python must be installed on your server for this to work _sudo apt-get install python_
* Installs and configures Bind9
* Installs and sets UFW to allow SSH and Port 53 (doesn't do any other changes)

## Debian 10
* Installs and configures Bind9
* Installs and sets UFW to allow SSH and Port 53 (doesn't do any other changes)

## Devuan 3
* Installs and configures Bind9
* Installs and sets UFW to allow SSH and Port 53 (doesn't do any other changes)

## To-Do
* Devuan 4 Support
